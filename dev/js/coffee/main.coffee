
###create nav slider###
countSlide = $(".slider-for .item").length;
slider = $(".slider-for").html()
$(".slider-nav").html(slider)

###init slider###
$('.slider-for').slick
  slidesToShow: 1
  slidesToScroll: 1
  arrows: false
  fade: true
  asNavFor: '.slider-nav'
  dots: true
$('.slider-nav').slick
  slidesToShow: 4
  slidesToScroll: 1
  asNavFor: '.slider-for'
  arrows: false
  dots: false
  focusOnSelect: true

#firefox padding placeholder###
if(navigator.userAgent.indexOf("Firefox") != -1 )
  $(".search__input").addClass("input-firefox")

###login button###
$('.login__button').click ->
  $(".login").toggleClass("login__active");
  $(".papab").toggleClass("papab__active");
  return false

###open catalog###
save = 0
$('.catalog__link').click ->
  open = $(this).data("open");
  if (save is open)
    $(this).toggleClass("catalog__link-active")
    $("#more-info__"+save).toggleClass("more-info__open")
  else
    $('.catalog__link').removeClass("catalog__link-active")
    $("#more-info__"+save).removeClass("more-info__open")
    $(this).addClass("catalog__link-active")
    $("#more-info__"+open).addClass("more-info__open")
  save = open
  return false

###fix menu nav css###
switch
  when (countSlide is 4)
    $(".slick-initialized.slider-nav .slick-slide").addClass("fix-width")
  when (countSlide is 3)
    $(".slick-initialized.slider-nav .slick-slide").addClass("fix-width-2")
    $(".slider-nav .slick-track").addClass("fix-track")
  when (countSlide <= 2)
    $('.slider-nav').html("")