
/*create nav slider */

(function() {
  var countSlide, save, slider;

  countSlide = $(".slider-for .item").length;

  slider = $(".slider-for").html();

  $(".slider-nav").html(slider);


  /*init slider */

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    dots: true
  });

  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    arrows: false,
    dots: false,
    focusOnSelect: true
  });

  if (navigator.userAgent.indexOf("Firefox") !== -1) {
    $(".search__input").addClass("input-firefox");
  }


  /*login button */

  $('.login__button').click(function() {
    $(".login").toggleClass("login__active");
    $(".papab").toggleClass("papab__active");
    return false;
  });


  /*open catalog */

  save = 0;

  $('.catalog__link').click(function() {
    var open;
    open = $(this).data("open");
    if (save === open) {
      $(this).toggleClass("catalog__link-active");
      $("#more-info__" + save).toggleClass("more-info__open");
    } else {
      $('.catalog__link').removeClass("catalog__link-active");
      $("#more-info__" + save).removeClass("more-info__open");
      $(this).addClass("catalog__link-active");
      $("#more-info__" + open).addClass("more-info__open");
    }
    save = open;
    return false;
  });


  /*fix menu nav css */

  switch (false) {
    case !(countSlide === 4):
      $(".slick-initialized.slider-nav .slick-slide").addClass("fix-width");
      break;
    case !(countSlide === 3):
      $(".slick-initialized.slider-nav .slick-slide").addClass("fix-width-2");
      $(".slider-nav .slick-track").addClass("fix-track");
      break;
    case !(countSlide <= 2):
      $('.slider-nav').html("");
  }

}).call(this);
